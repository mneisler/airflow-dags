"""
Loads wmf.pageview_hourly from Hive to druid datasource pageviews_hourly.

The loading is done with 2 dags: an hourly and daily dag.
The hourly DAG loads data to Druid as soon as it's available in Hive.
The daily DAG waits for a full day's data to be available in Hive,
and loads it all to Druid as a single daily segment.


All 2 DAGs reads from hive wmf.pageview_hourly table.
Druid datasource is pageviews_hourly
"""

from datetime import datetime, timedelta

from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor,
)

from analytics.config.dag_config import (
    alerts_email,
    artifact,
    create_easy_dag,
    dataset,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.druid import HiveToDruidOperator
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator

dag_id = "druid_load_pageviews_hourly"
props = DagProperties(
    # input tables
    hive_pageview_table="wmf.pageview_hourly",
    wiki_map_table="canonical_data.wikis",
    namespace_map_table="wmf_raw.mediawiki_project_namespace_map",
    # Project-namespace-map-table snapshot
    # Project-namespace-map is made available for month X-1 after
    # all mediawiki tables are sqooped at the beginning of month X,
    # meaning around the 2nd at mid-day (currently).
    # We can't accept that much delay for pageviews to druid, so we
    # need to delay the data by some days (eg 10 days) and use
    # ie at day 2020-04-01, this job will calculate data for an hour or a day
    # by using Project-namespace-map snapshot generated from the
    # previous month of [2020-04-01]-10days
    mw_namespace_snapshot="{{data_interval_start|subtract_days(10)|start_of_previous_month|to_ds_month}}",
    # temporary database and base directory.
    temporary_directory=f"{hadoop_name_node}/wmf/tmp/druid",
    temporary_database="wmf",
    # start dates
    hourly_start_date=datetime(2023, 4, 1, 0),
    daily_start_date=datetime(2023, 4, 1),
    # hql file location
    hourly_hql_path=f"{hql_directory}/druid_load/pageview/generate_hourly_druid_pageviews.hql",
    daily_hql_path=f"{hql_directory}/druid_load/pageview/generate_daily_druid_pageviews.hql",
    # druid config
    common_hive_to_druid_config={
        "druid_datasource": "pageviews_hourly",
        "timestamp_column": "dt",
        "timestamp_format": "auto",
        "dimensions": [
            "project",
            "language_variant",
            "project_family",
            "namespace_is_content",
            "namespace_is_talk",
            "namespace_canonical_name",
            "access_method",
            "agent_type",
            "referer_class",
            "continent",
            "country_code",
            "country",
            "subdivision",
            "city",
            "ua_device_family",
            "ua_browser_family",
            "ua_browser_major",
            "ua_os_family",
            "ua_os_major",
            "ua_os_minor",
            "ua_wmf_app_version",
            "referer_name",
        ],
        "metrics": ["view_count"],
        "query_granularity": "hour",
        "reduce_memory": "8192",
        "hadoop_queue": "production",
        "hive_to_druid_jar": artifact("refinery-job-0.2.14-shaded.jar"),
        "temp_directory": None,  # Override just for testing.
    },
    # Configs for running very simple SparkSQL code from the Skein app.
    minimalist_spark_config={
        "master": "local",
        "driver_cores": 1,
        "driver_memory": "1G",
    },
    # SLA and alerts email.
    sla=timedelta(hours=6),
    alerts_email=alerts_email,
)

default_tags = ["from_hive", "to_druid", "uses_hql", "uses_spark", "requires_wmf_pageview_hourly"]

# Hourly Dag
with create_easy_dag(
    dag_id=dag_id,
    doc_md="Loads pageview from hive to druid hourly",
    start_date=props.hourly_start_date,
    schedule="@hourly",
    tags=["hourly"] + default_tags,
    sla=props.sla,
    email=props.alerts_email,
) as hourly_dag:
    hourly_temporary_table = "tmp_{{dag.dag_id}}_{{data_interval_start|to_ds_hour_nodash}}"
    hourly_temporary_directory = props.temporary_directory + "/{{dag.dag_id}}_{{data_interval_start|to_ds_hour_nodash}}"

    sensors = []
    sensors.append(dataset("hive_wmf_pageview_hourly").get_sensor_for(hourly_dag))
    sensors.append(
        NamedHivePartitionSensor(
            task_id="wait_for_mediawiki_project_namespace_map",
            partition_names=[f"{props.namespace_map_table}/snapshot={props.mw_namespace_snapshot}"],
            poke_interval=timedelta(minutes=15).total_seconds(),
        )
    )
    format_pageview = SparkSqlOperator(
        task_id="format_hourly_pageview",
        sql=props.hourly_hql_path,
        query_parameters={
            "source_table": props.hive_pageview_table,
            "wiki_map_table": props.wiki_map_table,
            "namespace_map_table": props.namespace_map_table,
            "destination_directory": hourly_temporary_directory,
            "destination_table": f"{props.temporary_database}.{hourly_temporary_table}",
            "mediawiki_snapshot": props.mw_namespace_snapshot,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
            "hour": "{{ data_interval_start.hour }}",
            "coalesce_partitions": 4,
        },
    )

    druid_loader = HiveToDruidOperator(
        task_id="load_to_druid_hourly",
        database=props.temporary_database,
        table=hourly_temporary_table,
        since="{{data_interval_start | to_ds_hour}}",
        until="{{data_interval_start | add_hours(1) | to_ds_hour}}",
        segment_granularity="hour",
        num_shards=2,
        executor_cores=4,
        executor_memory="8G",
        **props.common_hive_to_druid_config,
    )

    hive_cleaner = SparkSqlOperator(
        task_id="remove_temporary_table",
        sql=f"DROP TABLE IF EXISTS {props.temporary_database}.{hourly_temporary_table};",
        **props.minimalist_spark_config,
    )

    hdfs_cleaner = SimpleSkeinOperator(
        task_id="remove_temporary_directory",
        script=f"hdfs dfs -rm -r {hourly_temporary_directory}",
    )

    sensors >> format_pageview >> druid_loader >> hive_cleaner >> hdfs_cleaner

# Daily Dag
with create_easy_dag(
    dag_id=f"{dag_id}_aggregated_daily",
    doc_md="Loads pageview from hive to druid daily",
    start_date=props.daily_start_date,
    schedule="@daily",
    tags=["daily"] + default_tags,
    sla=props.sla,
    email=props.alerts_email,
) as daily_dag:
    daily_temporary_directory = props.temporary_directory + "/{{dag.dag_id}}_{{data_interval_start|to_ds_nodash}}"
    daily_temporary_table = "tmp_{{dag.dag_id}}_{{data_interval_start|to_ds_nodash}}"

    sensors = []
    sensors.append(dataset("hive_wmf_pageview_hourly").get_sensor_for(daily_dag))
    sensors.append(
        NamedHivePartitionSensor(
            task_id="wait_for_mediawiki_project_namespace_map",
            partition_names=[f"{props.namespace_map_table}/snapshot={props.mw_namespace_snapshot}"],
            poke_interval=timedelta(minutes=15).total_seconds(),
        )
    )

    format_pageview = SparkSqlOperator(
        task_id="format_daily_pageview",
        sql=props.daily_hql_path,
        query_parameters={
            "source_table": props.hive_pageview_table,
            "wiki_map_table": props.wiki_map_table,
            "namespace_map_table": props.namespace_map_table,
            "destination_directory": daily_temporary_directory,
            "destination_table": f"{props.temporary_database}.{daily_temporary_table}",
            "mediawiki_snapshot": props.mw_namespace_snapshot,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
            "coalesce_partitions": 8,
        },
        executor_memory="8G",
        executor_cores=2,
        conf={
            "spark.dynamicAllocation.maxExecutors": 64,
            "spark.yarn.executor.memoryOverhead": 2048,
        },
    )

    druid_loader = HiveToDruidOperator(
        task_id="load_to_druid_daily",
        database=props.temporary_database,
        table=daily_temporary_table,
        since="{{data_interval_start | to_ds_hour}}",
        until="{{data_interval_start | add_days(1) | to_ds_hour}}",
        segment_granularity="day",
        num_shards=8,
        executor_cores=4,
        executor_memory="8G",
        **props.common_hive_to_druid_config,
    )

    hive_cleaner = SparkSqlOperator(
        task_id="remove_temporary_table",
        sql=f"DROP TABLE IF EXISTS {props.temporary_database}.{daily_temporary_table};",
        **props.minimalist_spark_config,
    )

    hdfs_cleaner = SimpleSkeinOperator(
        task_id="remove_temporary_directory",
        script=f"hdfs dfs -rm -r {daily_temporary_directory}",
    )

    sensors >> format_pageview >> druid_loader >> hive_cleaner >> hdfs_cleaner
