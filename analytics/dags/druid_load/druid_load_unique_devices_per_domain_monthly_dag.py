"""
Formats wmf.unique_devices_per_domain_monthly
and loads it to Druid at a monthly interval.
"""

from datetime import datetime, timedelta

from analytics.config.dag_config import (
    alerts_email,
    artifact,
    create_easy_dag,
    dataset,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.druid import HiveToDruidOperator
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator

props = DagProperties(
    # DAG start date.
    start_date=datetime(2023, 4, 1),
    # Source database and table.
    unique_devices_database="wmf",
    unique_devices_table="unique_devices_per_domain_monthly",
    # HQL query paths.
    druid_hql_path=(hql_directory + "/druid_load/generate_druid_unique_devices_per_domain_monthly.hql"),
    # Intermediate (temporary) database and base directory.
    intermediate_database="tmp",
    intermediate_base_directory=f"{hadoop_name_node}/wmf/tmp/druid",
    # Druid configs common to both DAGs.
    hive_to_druid_config={
        "druid_datasource": "unique_devices_per_domain_monthly",
        "timestamp_column": "dt",
        "timestamp_format": "auto",
        "dimensions": [
            "domain",
            "country",
            "country_code",
        ],
        "metrics": [
            "uniques_underestimate",
            "uniques_offset",
            "uniques_estimate",
        ],
        "query_granularity": "month",
        "segment_granularity": "month",
        "num_shards": 1,
        "reduce_memory": 8192,
        "hadoop_queue": "production",
        "hive_to_druid_jar": artifact("refinery-job-0.2.14-shaded.jar"),
        # NOTE: for testing, set to a directory whose parent is owned by analytics-privatedata:druid
        "temp_directory": None,
    },
    # Configs for running very simple SparkSQL code from the Skein app.
    minimalist_spark_config={
        "master": "local",
        "driver_cores": 1,
        "driver_memory": "1G",
    },
    # SLA and alerts email.
    sla=timedelta(days=1),
    alerts_email=alerts_email,
)


with create_easy_dag(
    dag_id="druid_load_unique_devices_per_domain_monthly",
    doc_md="Loads unique devices per domain from Hive to Druid monthly.",
    start_date=props.start_date,
    schedule="@monthly",
    tags=[
        "monthly",
        "from_hive",
        "to_druid",
        "uses_hql",
        "uses_spark",
        "requires_wmf_unique_devices_per_domain_monthly",
    ],
    sla=props.sla,
    email=props.alerts_email,
) as monthly_dag:
    # Table and directory where to store the temporary data to be loaded to Druid.
    intermediate_table = "tmp_{{dag.dag_id}}_{{data_interval_start|to_ds_month_nodash}}"
    intermediate_directory = (
        props.intermediate_base_directory + "/{{dag.dag_id}}/{{data_interval_start|to_ds_month_nodash}}"
    )

    sensor = dataset("hive_wmf_unique_devices_per_domain_monthly").get_sensor_for(monthly_dag)

    formatter = SparkSqlOperator(
        task_id="format_data_to_load",
        sql=props.druid_hql_path,
        query_parameters={
            "source_table": f"{props.unique_devices_database}.{props.unique_devices_table}",
            "destination_table": f"{props.intermediate_database}.{intermediate_table}",
            "destination_directory": intermediate_directory,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
        },
    )

    loader = HiveToDruidOperator(
        task_id="load_to_druid",
        database=props.intermediate_database,
        table=intermediate_table,
        since="{{data_interval_start | to_ds_hour}}",
        until="{{data_interval_end | to_ds_hour}}",
        **props.hive_to_druid_config,
    )

    hive_cleaner = SparkSqlOperator(
        task_id="remove_temporary_table",
        sql=f"DROP TABLE IF EXISTS {props.intermediate_database}.{intermediate_table};",
        **props.minimalist_spark_config,
    )

    hdfs_cleaner = SimpleSkeinOperator(
        task_id="remove_temporary_directory",
        script=f"hdfs dfs -rm -r {intermediate_directory}",
    )

    sensor >> formatter >> loader >> [hive_cleaner, hdfs_cleaner]
