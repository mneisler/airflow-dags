from airflow.providers.apache.hive.hooks.hive import HiveMetastoreHook
from typing import cast, Any, Optional

class HiveTablePath:
    """Returns the location in storage of a hive table.

    To use add as a filter in the dag:

        with DAG('example_dag',
            user_defined_filters={
                'hive_table_path': HiveTablePath('metastore_default'),
            }
        ) as dag:

    The filter can then be applied to a string containg the
    fully qualified table name:

        {{ "my_db.my_table" | hive_table_path }}

    This filter can be short-circuited, if for example the parameter was
    fetched from configuration, by passing an hdfs path. In this case the
    provided path will be returned as-is:

        {{ "hdfs://somewhere/to/load/data" | hive_table_path }}


    """
    def __init__(self, metastore_conn_id: str):
        self.metastore_conn_id = metastore_conn_id
        self.hook = cast(Optional[HiveMetastoreHook], None)

    def _table(self, qualified_table: str) -> Any:
        if '.' not in qualified_table:
            raise ValueError(f'table must be fully qualified [{qualified_table}]')
        if self.hook is None:
            self.hook = HiveMetastoreHook(self.metastore_conn_id)
        database_name, table_name = qualified_table.split('.', 2)
        return self.hook.get_table(db=database_name, table_name=table_name)

    def __call__(self, qualified_table: str) -> str:
        if qualified_table.startswith('hdfs://'):
            return qualified_table
        return cast(str, self._table(qualified_table).sd.location)
