from typing import Any, List

from datahub_provider.operators.datahub import DatahubEmitterOperator

from wmf_airflow_common.dataset import Dataset
from wmf_airflow_common.util import resolve_kwargs_default_args


def build_task_id(downstream: Dataset) -> str:
    return f"emit_lineage_to_datahub_for_{downstream.name_snakecase()}"


class DatahubLineageEmitterOperator(DatahubEmitterOperator):
    """A proxy Operator to emit lineage information to DataHub"""

    def __init__(self, downstream: Dataset, upstreams: List[Dataset], **kwargs: Any):
        """
        Creates an DataHub Lineage Emitter Operator.

        :param downstream: The downstream dataset.
        :param upstreams: A list of upstream datasets.
        """
        kwargs["task_id"] = kwargs.get("task_id", build_task_id(downstream))
        kwargs["mces"] = [
            downstream.datahub_upstream_lineage_mce(
                upstreams, resolve_kwargs_default_args(kwargs, "datahub_environment")
            )
        ]
        super().__init__(**kwargs)
