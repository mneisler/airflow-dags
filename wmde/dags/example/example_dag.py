"""
Example DAG for reference purposes.
It should execute a Spark SQL query that reads from a Hive table
and writes the results into another hive table, on a daily basis.
Please, don't try to execute it, since the references are all fake.
"""

from datetime import datetime, timedelta

from wmde.config.dag_config import alerts_email, create_easy_dag, dataset
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

# Put here all the properties you want to control from Airflow UI.
# See: wmf_airflow_common/config/dag_properties.py.
props = DagProperties(
    start_date=datetime(2023, 8, 1),
    hql_uri=(
        # This could be any static URL that returns the HQL query to return.
        "https://gitlab.wikimedia.org/repos/some-team/some-repo"
        "/-/raw/4c3e9fd235ad44b50839c2d8781f896cd73c4d0b"
        "/some-project/some-query.hql"
    ),
    pageview_hourly_table="wmf.pageview_hourly",
    example_output_table="wmde.output_table",
    sla=timedelta(hours=6),
    alerts_email=alerts_email,
)


# This creates a regular Airflow DAG through a more convenient syntax.
with create_easy_dag(
    dag_id="a_unique_name_for_your_dag",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@daily",
    sla=props.sla,
    email=props.alerts_email,
) as dag:
    # This waits for the source data to be present before executing the compute.
    sensor = dataset("hive_wmf_pageview_hourly").get_sensor_for(dag)

    # This executes the Spark SQL query.
    compute = SparkSqlOperator(
        task_id="compute_something",
        sql=props.hql_uri,
        query_parameters={
            # Your query should accept these parameters
            # using Spark placeholders, i.e. ${source_table}, etc.
            "source_table": props.pageview_hourly_table,
            "destination_table": props.example_output_table,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
            "day": "{{data_interval_start.day}}",
        },
        # Spark configuration.
        driver_cores=2,
        driver_memory="4G",
        executor_cores=4,
        executor_memory="8G",
        conf={"spark.dynamicAllocation.maxExecutors": 16, "spark.yarn.executor.memoryOverhead": 2048},
    )

    # This specifies the order/dependencies of the tasks above.
    sensor >> compute
