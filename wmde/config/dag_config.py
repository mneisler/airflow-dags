from os import environ, path

from wmf_airflow_common.artifact import ArtifactRegistry
from wmf_airflow_common.config import dag_default_args
from wmf_airflow_common.dataset import DatasetRegistry
from wmf_airflow_common.easy_dag import EasyDAGFactory
from wmf_airflow_common.templates.time_filters import filters
from wmf_airflow_common.util import is_wmf_airflow_instance

# General paths.
hadoop_name_node = "hdfs://analytics-hadoop"
hdfs_temp_directory = "/wmf/tmp/wmde"

# Emails.
# Temporary until WMDE can provide a team alerts email.
alerts_email = "manuel.merz@wikimedia.de"

# default_args for this Airflow Instance.
instance_default_args = {
    "owner": "analytics-wmde",
    "email": alerts_email,
    "hadoop_name_node": hadoop_name_node,
    "metastore_conn_id": "analytics-hive",
}

# For jobs running in production instances, set the production yarn queue.
if is_wmf_airflow_instance():
    instance_default_args["queue"] = "production"

# Artifact registry for dependency paths.  By renaming
# this function to 'artifact', we get a nice little
# syntactic sugar to use when declaring artifacts in dags.
artifact_registry = ArtifactRegistry.for_wmf_airflow_instance("wmde")
artifact = artifact_registry.artifact_url

# Dataset registry for easier Sensor configuration.
# Provides a syntactic sugar to use when creating Sensors in DAGs.
dataset_file_paths = [path.join(path.dirname(__file__), "datasets.yaml")]
dataset_registry = DatasetRegistry(dataset_file_paths)
dataset = dataset_registry.get_dataset

# Default arguments for all operators used by this airflow instance.
default_args = dag_default_args.get(
    {
        **instance_default_args,
        # Arguments for operators that have artifact dependencies
        "hdfs_tools_shaded_jar_path": artifact("hdfs-tools-0.0.6-shaded.jar"),
        "spark_sql_driver_jar_path": artifact("wmf-sparksqlclidriver-1.0.0.jar"),
    }
)

if not is_wmf_airflow_instance():
    default_args["owner"] = environ.get("USER")

# Helper for creating DAGs with a nicer interface.
# Passes all the defaults and shortcuts to the DAG factory.
create_easy_dag = EasyDAGFactory(
    factory_default_args={
        **default_args,
        # Add here more default args dictionaries you want the easy_dag to have.
    },
    factory_default_args_shortcuts=["email", "sla"],
    factory_user_defined_filters=filters,
).create_easy_dag
