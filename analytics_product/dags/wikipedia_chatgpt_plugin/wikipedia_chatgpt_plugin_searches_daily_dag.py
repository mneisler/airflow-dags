"""
Computes wikipedia_chatgpt_plugin_searches data daily.

* Waits for the corresponding mediawiki_cirrussearch_request partitions to be available.
* Populates the destination partition using a SparkSQL query file.
"""

from analytics_product.config.dag_config import alerts_email, dataset, create_easy_dag
from datetime import datetime, timedelta
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator


props = DagProperties(
    start_date=datetime(2023, 6, 24),
    hql_uri=(
        "https://gitlab.wikimedia.org/repos/product-analytics/data-pipelines"
        "/-/raw/40fcc4126c1029a94992cef3c74600ef92741ec8"
        "/wikipedia_chatgpt_plugin/searches/generate_wikipedia_chatgpt_plugin_searches_daily.hql"
    ),
    mediawiki_cirrussearch_request_table="event.mediawiki_cirrussearch_request",
    wikipedia_chatgpt_plugin_searches_table="wmf_product.wikipedia_chatgpt_plugin_searches",
    sla=timedelta(hours=6),
    alerts_email=alerts_email
)


with create_easy_dag(
    dag_id="wikipedia_chatgpt_plugin_searches_daily",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@daily",
    sla=props.sla,
    email=props.alerts_email
) as dag:

    sensor = dataset("hive_event_mediawiki_cirrussearch_request").get_sensor_for(dag)

    compute = SparkSqlOperator(
        task_id="compute_wikipedia_chatgpt_plugin_searches",
        sql=props.hql_uri,
        query_parameters={
            "source_table": props.mediawiki_cirrussearch_request_table,
            "destination_table": props.wikipedia_chatgpt_plugin_searches_table,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
            "day": "{{data_interval_start.day}}"
        },
        driver_cores=2,
        driver_memory="4G",
        executor_cores=4,
        executor_memory="8G",
        conf={
            "spark.dynamicAllocation.maxExecutors": 16,
            "spark.yarn.executor.memoryOverhead": 2048
        }
    )

    sensor >> compute
