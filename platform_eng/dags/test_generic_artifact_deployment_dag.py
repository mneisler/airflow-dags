from datetime import datetime

from airflow import DAG

from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

from platform_eng.config.dag_config import default_args

dag_id = 'test_generic_artifact_deployment_dag'
var_props = VariableProperties(f'{dag_id}_config')

with DAG(
        dag_id=dag_id,
        start_date=var_props.get_datetime('start_date', datetime(2023, 4, 12)),
        schedule='@daily',
        max_active_runs=1,
        tags=['test'],
        default_args=default_args
) as dag:

    do_hql = SparkSqlOperator(
        task_id="do_hql",
        # To run an HQL file, simply use a GitLab's raw URI that points to it.
        # See how to build such a URI here:
        # https://docs.gitlab.com/ee/api/repository_files.html#get-raw-file-from-repository
        # We strongly recommend you use an immutable URI (i.e. one that includes an SHA or a tag) for reproducibility
        sql=var_props.get(
            'hql_gitlab_raw_path',
            'https://gitlab.wikimedia.org/api/v4/projects/1261/repository/files/test%2Ftest_hql.hql/raw?ref=0e4d2a9'
        ),
        query_parameters={
            'destination_directory': f'/tmp/xcollazo_test_generic_artifact_deployment_dag/{{{{ts_nodash}}}}',
            'snapshot': '2023-01-02',
        },
        launcher='skein',
    )

    do_hql
