from pathlib import Path

import pytest
from airflow.models import Pool
from airflow.providers.apache.hdfs.sensors.hdfs \
    import HdfsSensor
from airflow.providers.apache.spark.operators.spark_submit \
    import SparkSubmitOperator as AirflowSparkSubmitOperator
from airflow.sensors.external_task import ExternalTaskSensor

from search.shared.utils import parse_memory_to_mb
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.hive import RangeHivePartitionSensor


def all_dag_paths():
    test_path = Path(__file__)
    instance_name = test_path.parts[-2]
    base_dir = test_path.parents[2]
    dags_dir = Path(base_dir, instance_name, 'dags')
    return [
        list(path.relative_to(base_dir).parts)
        for path in dags_dir.glob('**/*.py')
    ]


@pytest.mark.parametrize('dag_path', all_dag_paths())
def test_clean_import(dagbag):
    assert dagbag.import_errors == {}
    assert len(dagbag.dag_ids) > 0


@pytest.mark.for_each_task()
def test_tasks_are_renderable(task, render_task):
    # Nothing specific to test here, simply rendering the task is sufficient
    # to verify tasks are renderable. On failure jinja2.exceptions.TemplateSyntaxError will
    # be thrown from render_task.
    render_task(task)


@pytest.mark.for_each_task(kind=AirflowSparkSubmitOperator)
def test_dont_use_upstream_spark_submit(task):
    assert isinstance(task, SparkSubmitOperator), \
        'Tasks must use the wmf SparkSubmitOperator implementation'


def _sort_items_recursive(maybe_dict):
    """Recursively sort dictionaries so iteration gives deterministic outputs"""
    if hasattr(maybe_dict, 'items'):
        items = ((k, _sort_items_recursive(v)) for k, v in maybe_dict.items())
        return dict(sorted(items, key=lambda x: x[0]))
    else:
        return maybe_dict


@pytest.mark.for_each_task(kind=SparkSubmitOperator)
def test_spark_submit_uses_standard_arguments(task):
    """Test that spark operators uses same arguments

    There are parts of spark that can be configured through conf or through
    spark-submit cli args. Prefer cli args for consistency.
    """
    assert 'spark.executor.memory' not in task._conf, \
        'Executor memory must be configured through kwarg'
    assert 'spark.executor.cores' not in task._conf, \
        'Executor cores must be configured through kwarg'


@pytest.mark.for_each_task(kind=SparkSubmitOperator)
def test_spark_submit_sizing(task):
    max_exec = int(task._conf['spark.dynamicAllocation.maxExecutors'])
    exec_mem_gb = parse_memory_to_mb(task._executor_memory if task._executor_memory is not None else '1G') / 1024
    max_mem_gb = max_exec * exec_mem_gb
    if max_mem_gb > 420:
        assert task.pool != Pool.DEFAULT_POOL_NAME, \
            'Large job (mem > 420g) should not be using the default pool'

    if max_mem_gb > 800:
        assert task.pool == 'sequential', \
            'Large job (mem > 800g) should be using the sequential pool'

    exec_cores = int(task._executor_cores if task._executor_cores is not None else '1')
    task_cores = int(task._conf['spark.task.cpus'] if 'spark.task.cpus' in task._conf else '1')

    assert (exec_cores % task_cores) == 0, \
        'executor_cores should be a multiple of spark.task.cpus'
    assert (exec_cores / task_cores) <= exec_mem_gb, \
        'executor_memory looks suspiciously low (less than 1G per executor task)'

EMAIL_WHITELIST = {
    'discovery-alerts@lists.wikimedia.org',
    'sd-alerts@lists.wikimedia.org'
}


@pytest.mark.for_each_task()
def test_task_email_is_whitelisted(task):
    """Help prevent typos in alerting emails"""
    for email in task.email:
        assert email in EMAIL_WHITELIST


@pytest.mark.for_each_task(kind=ExternalTaskSensor)
def test_external_task_exists(task, instance_tasks):
    all_dag_ids = {meta.dag_id for meta in instance_tasks}
    assert task.external_dag_id in all_dag_ids
    dag_task_ids = {
        meta.task_id for meta in instance_tasks
        if meta.dag_id == task.external_dag_id
    }
    assert task.external_task_id in dag_task_ids


@pytest.mark.for_each_task(kind=HdfsSensor)
def test_dont_use_hdfs_sensor(task):
    assert False, "Upstream HdfsSensor is unusable, " \
                  "use wmf_airflow_common.sensors.url.URLSensor"


@pytest.mark.for_each_task(kind=RangeHivePartitionSensor)
def test_range_sensor_pre_execute(task, render_task):
    rendered_task = render_task(task)
    # Nothing specific to test, bad parameters will throw an exception
    rendered_task.pre_execute()
